<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php echo $this->fetch('title'); ?>
		</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta charset="UTF-8" />
		
		<?php
			echo $this->Html->meta('icon');

			echo $this->Html->css([
				'/bower_components/bootstrap/dist/css/bootstrap.min.css',
				'/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
				'/bower_components/bootstrap-fileinput/css/fileinput.min.css',
				'/bower_components/font-awesome/css/font-awesome.min.css',
				'/bower_components/bootswatch/slate/bootstrap.min.css',
				'/bower_components/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
				'/bower_components/multiselect/css/multi-select.css'
			]);

			echo $this->fetch('meta');
			echo $this->fetch('css');
		?>
		<style>
			.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
				vertical-align: middle;
				text-align: center;
			}
			.table>tbody>tr>td>img {
				margin: auto;
			}
			.ms-container {width: 100%;}
		</style>
	</head>
	<body>

		<?php echo $this->Element("navbar")?>

		<main class="container-fluid">

			<section>
				<?php echo $this->Session->flash("flash", ["element" => "flash"]); ?>
				<?php echo $this->Session->flash("auth", ["element" => "flash"]); ?>
				<?php echo $this->fetch('content'); ?>
			</section>

		</main>

		<div class="modal fade" id="modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				</div>
			</div>
		</div>		

		<?php 
		echo $this->Html->script([
			'/bower_components/jquery/dist/jquery.min.js',
			'/bower_components/bootstrap/dist/js/bootstrap.min.js',
			'/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
			'/bower_components/bootstrap-fileinput/js/fileinput.min.js',
			#'/bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect.js',
			'/bower_components/multiselect/js/jquery.multi-select.js',
		]);
		?>
		<script>
		$(function(){
			$.fn.datepicker.defaults.format = 'yyyy-mm-dd';
			$("input[type='file']").fileinput({'showUpload':false});
			$("select[multiple='multiple']").multiSelect();
			$("*[data-toggle='tooltip']").tooltip();
		});
		</script>
		
		<?php echo $this->fetch('script');?>

	</body>
</html>