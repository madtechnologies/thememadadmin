<div class="row">

	<div class="col-sm-12">

		<h2><?php echo $this->fetch("title")?>: Agregar</h2>

		<ol class="breadcrumb">
			<li><a href="<?php echo Router::url("/admin")?>">Admin</a></li>
			<li>
				<a href="<?php echo Router::url(["action" => "index"])?>"><?php echo $this->fetch("title")?></a>
			</li>
			<li class="active">Agregar</li>
		</ol>

	</div>

	<aside class="col-sm-2">
		<div class="list-group">
			<strong class="list-group-item active">Menu</strong>
			<?php echo $this->fetch("aside")?>
		</div>		
	</aside>

	<section class="col-sm-10">
		<?php echo $this->fetch("content")?>
	</section>

</div>