<div class="row">

	<div class="col-sm-12">

		<h2><?php echo $this->fetch("title")?>: Listar</h2>

		<ol class="breadcrumb">
			<li><a href="<?php echo Router::url("/admin")?>">Admin</a></li>
			<li>
				<a href="<?php echo Router::url(["action" => "index"])?>"><?php echo $this->fetch("title")?></a>
			</li>
			<li class="active">Listar</li>
		</ol>

	</div>

	<aside class="col-sm-2">
		<div class="list-group">
			<strong class="list-group-item active">Menu</strong>
			<?php echo $this->fetch("aside")?>
		</div>		
	</aside>

	<section class="col-sm-10">

		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<?php echo $this->fetch("tHead")?>
						<th>
							Acciones
						</th>
					</tr>
				</thead>
				<tbody>
					<?php echo $this->fetch("tBody")?>
				</tobdy>
			</table>
		</div>

		<div class="text-center">
			<ul class="pagination">
				<?php echo $this->Paginator->prev('«');?>
				<?php echo $this->Paginator->numbers();?>
				<?php echo $this->Paginator->next('»');?>
			</ul>
		</div>	

	</section>

</div>